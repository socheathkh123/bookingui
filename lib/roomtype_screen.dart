
import 'package:flutter/material.dart';

import 'models/hotel.dart';
import 'my_account_page.dart';
import 'navigation_drawer_screen.dart';

class RoomType extends StatefulWidget {
  const RoomType({Key? key}) : super(key: key);

  @override
  _RoomTypeState createState() => _RoomTypeState();
}

class _RoomTypeState extends State<RoomType> {
  var kDefaultPaddin;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
        elevation: 0.1,
        backgroundColor: Color(0xFF263171),
          title: Text('ROOM TYPE',
            style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Color(0xFFECB52E)
            ),
          ),
          actions: <Widget>[
            new IconButton(
                icon: Icon(Icons.search, color: Colors.white,), onPressed: () {}),
            new IconButton(icon: Icon(Icons.shopping_cart, color: Colors.white,),
                onPressed: () {})
          ],
        ),
    body: ListView(
    children: <Widget>[
      SizedBox(
        height: 10.0,
      ),
      Container(
        height: 220,
        width: double.infinity,
          child: GridView.builder(
            itemCount: hotels.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              //mainAxisSpacing: kDefaultPaddin,
              //crossAxisSpacing: kDefaultPaddin,
              childAspectRatio: 0.75,
            ),
            itemBuilder: (BuildContext context, index) {
              Hotel hotel = hotels[index];
              return GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => MyAccountPage(
                        //hotel: hotel,
                      ),
                    ),
                  );
                },
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 5.0),
                height: 200,
                width: 170,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      offset: Offset(0.0, 4.0),
                      blurRadius: 10.0,
                    )
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 140.0,
                      width: 170.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10.0),
                          topRight: Radius.circular(10.0),
                        ),
                        image: DecorationImage(
                          image: AssetImage(hotels[index].imageUrl),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 5, top: 2),
                      child: Text(
                        hotels[index].title,
                        style: TextStyle(fontSize: 16.0),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 5.0, right: 5.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            hotels[index].description,
                            style: TextStyle(fontSize: 12.0),
                          ),
                          Row(
                            children: <Widget>[
                              Text(
                                hotels[index].description,
                                style: TextStyle(fontSize: 12.0),
                              ),

                            ],
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 5),
                      child: Text(
                        '\$${hotels[index].price} / night',
                        style: TextStyle(color: Colors.blue),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 5.0, right: 5.0,top: 3),
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.directions_car,
                            color: Color(0xFF263171),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Icon(
                            Icons.hot_tub,
                            color: Color(0xFF263171),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Icon(
                            Icons.local_bar,
                            color: Color(0xFF263171),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Icon(
                            Icons.wifi,
                            color: Color(0xFF263171),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Icon(
                            Icons.star,
                            color: Color(0xFF263171),
                          ),
                          Positioned(
                            bottom: 50,
                            child: Container(
                              width: 96,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                color: Color(0xFF263171),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black26,
                                    blurRadius: 15.0,
                                    offset: Offset(2.0, 4.4),
                                  ),
                                ],
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(top: 0),
                                    child: Row(
                                      children: <Widget>[
                                        Icon(
                                          Icons.star,
                                          color: Color(0xFFECB52E),
                                        ),
                                        Icon(
                                          Icons.star,
                                          color: Color(0xFFECB52E),
                                        ),
                                        Icon(
                                          Icons.star,
                                          color: Color(0xFFECB52E),
                                        ),
                                        Icon(
                                          Icons.star,
                                          color: Color(0xFFECB52E),
                                        ),

                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            );
          },
        ),
      ),
      ],
    ),
    );
  }
}
