
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:splash_screen/models/room_model.dart';
import 'package:splash_screen/models/roomtype_model.dart';
import 'package:splash_screen/roomtype_screen.dart';

import 'models/hotel.dart';
import 'navigation_drawer_screen.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          elevation: 0.1,
          backgroundColor: Color(0xFF263171),
          title: Text('KATARI HOTEL',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Color(0xFFECB52E)
            ),
          ),
          actions: <Widget>[
            new IconButton(
                icon: Icon(Icons.search, color: Colors.white,), onPressed: () {}),
            new IconButton(icon: Icon(Icons.shopping_cart, color: Colors.white,),
                onPressed: () {})
          ],
        ),

        // body: container,
        drawer: Drawer(
          child: SingleChildScrollView(
            child: Container(
              child: Column(
                children: [
                  Navigetion(),
                  //MyDrawerList(),
                ],
              ),
            ),
          ),
        ),

        body: ListView(
        children: <Widget>[
        SizedBox(
        height: 200.0,
        child: Carousel(
          dotSize: 4.0,
          dotSpacing: 15.0,
          dotColor: Color(0xFF263171),
          indicatorBgPadding: 5.0,
          dotBgColor: Colors.transparent,
          dotVerticalPadding: 5.0,
          dotPosition: DotPosition.bottomCenter,
          autoplay: true,
          images: [
            Image.asset('images/1.png',fit: BoxFit.cover,),
            Image.asset('images/2.jpg',fit: BoxFit.cover,),
            Image.asset('images/3.png',fit: BoxFit.cover,),
          ],
        ),
        ),
          SizedBox(
            height: 15.0,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Room Type',
                  style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w600),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 15.0,
          ),
          Container(
            height: 160,
            width: double.infinity,
            child: ListView.builder(
              padding: EdgeInsets.symmetric(horizontal: 5.0),
              scrollDirection: Axis.horizontal,
              itemCount: roomtypes.length,
              itemBuilder: (BuildContext context, index) {
                Room roomtype = rooms[index];
                return GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => RoomType(

                        ),
                      ),
                    );
                  },
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 5.0),
                    height: 130,
                    width: 150,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.transparent,
                          offset: Offset(0.0, 4.0),
                          blurRadius: 10.0,
                        )
                      ],
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: 130.0,
                          width: 150.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10.0),
                              topRight: Radius.circular(10.0),
                            ),
                            image: DecorationImage(
                              image: AssetImage(roomtypes[index].imageUrl),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 5, top: 5),
                          child: Text(
                            roomtypes[index].RoomType,
                            style: TextStyle(fontSize: 14.0,fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
          SizedBox(
            height: 15.0,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Hotel Packages',
                  style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w600),
                ),
                Text(
                  'view all',
                  style: TextStyle(fontSize: 18.0, color: Colors.blue),
                )
              ],
            ),
          ),
          SizedBox(
            height: 15.0,
          ),
          Column(
            children: <Widget>[
              _hotelPackage(0),
              SizedBox(height: 10),
              _hotelPackage(1),
              SizedBox(height: 10),
              _hotelPackage(2),
              SizedBox(height: 10),
              _hotelPackage(3),
              SizedBox(height: 10),
              _hotelPackage(4),
              SizedBox(height: 10),
              _hotelPackage(5),
              SizedBox(height: 10),
              _hotelPackage(6),
              SizedBox(height: 10),
              _hotelPackage(7),
              SizedBox(height: 10),
              _hotelPackage(8),
              SizedBox(height: 10),
              _hotelPackage(9),
            ],
          ),
          SizedBox(
            height: 10.0,
          ),
      ]
      )
    );
  }
}
_hotelPackage(int index) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 10.0),
    child: Container(
      height: 120,
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.transparent,
            offset: Offset(0.0, 4.0),
            blurRadius: 10.0,
          )
        ],
      ),
      child: Stack(
        children: <Widget>[
          Positioned(
            child: Container(
              height: 120,
              width: 140,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10.0),
                  bottomLeft: Radius.circular(10.0),
                ),
                image: DecorationImage(
                  image: AssetImage(rooms[index].imageUrl),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Positioned(
            top: 3,
            right: 50,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  rooms[index].RoomType,
                  style: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 3,
                ),
                Text(
                  'RoomNumber ${rooms[index].RoomNumber}',
                  style: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  height: 3,
                ),
                Text(
                  'FloorNumber ${rooms[index].floor}',
                  style: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  height: 3,
                ),
                Text(
                  'Discount ${rooms[index].Discount} %',
                  style: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  height: 3,
                ),
                Text(
                  'Price ${rooms[index].price} / night',
                  style: TextStyle(fontSize: 14, color: Color(0xFFECB52E)),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 3),
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.directions_car,
                        color: Color(0xFF263171),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Icon(
                        Icons.hot_tub,
                        color: Color(0xFF263171),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Icon(
                        Icons.local_bar,
                        color: Color(0xFF263171),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Icon(
                        Icons.wifi,
                        color: Color(0xFF263171),
                      ),
                      SizedBox(
                        width: 10,
                      ),


                    ],
                  ),
                )
              ],
            ),
          ),
          Positioned(
            //top: 5,
            right: 2,
            bottom: 100,
            child: Container(
              width: 40,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5.0),
                color: Color(0xFF263171),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    blurRadius: 10.0,
                    offset: Offset(2.0, 4.4),
                  ),
                ],
              ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 6),
                  child: Row(
                    children: <Widget>[
                      Text(
                        '${rooms[index].rating}',
                        style: TextStyle(
                          fontSize: 12.0,
                          fontWeight: FontWeight.bold,
                          color: Color(0xFFECB52E),
                        ),
                      ),
                      Icon(
                        Icons.star,
                        color: Color(0xFFECB52E),
                        size: 12.0,
                      ),

                    ],
                  ),
                )
              ],
            ),
          ),
          ),
        ],
      ),
    ),
  );
}

