import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'login_screen.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  bool _hiden = true;
  late String userRegister;

  final usernameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmpasswordController = TextEditingController();

  @override
  void initState() {
    userRegister = "";
    super.initState();
  }
  @override
  void dispose() {
    usernameController.dispose();
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  void login(){
    setState(() {
      userRegister = "Username : "+usernameController.text + "Email : "+emailController.text;
    });
  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(padding: EdgeInsets.only(top: 50.0)),
                  Text(
                    "SING UP",
                    style: TextStyle(
                        fontSize: 20.0,
                        color: Color(0xFFECB52E),
                    ),
                  ),
                  const SizedBox(height: 40.0),
                  TextField(
                    controller: usernameController,
                    style: TextStyle(fontSize: 18.0),
                    decoration: InputDecoration(
                      suffixIcon: Icon(Icons.person),
                      hintText: 'Enter Your Username',
                      labelText: "Username",
                      border: OutlineInputBorder(),
                    ),
                  ),
                  const SizedBox(height: 14.0),
                  TextField(
                    controller: emailController,
                    style: TextStyle(fontSize: 18.0),
                    decoration: InputDecoration(
                      suffixIcon: Icon(Icons.person),
                      hintText: 'Enter Your Email',
                      labelText: "Email",
                      border: OutlineInputBorder(),
                    ),
                  ),
                  const SizedBox(height: 14.0),
                  TextField(
                    controller: passwordController,
                    obscureText: _hiden,
                    style: const TextStyle(fontSize: 18.0),
                    decoration: InputDecoration(
                      hintText: 'Enter Your Password',
                      labelText: "Password",
                      border: const OutlineInputBorder(),
                      suffixIcon: IconButton(
                        onPressed: () {
                          if (_hiden) {
                            setState(() => _hiden = false);
                          } else {
                            setState(() => _hiden = true);
                          }
                        },
                        icon: Icon(
                          _hiden ? Icons.visibility_off : Icons.visibility,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 14.0),
                  TextField(
                    controller: confirmpasswordController,
                    obscureText: _hiden,
                    style: const TextStyle(fontSize: 16.0),
                    decoration: InputDecoration(
                      hintText: 'Enter Your Confirm Password',
                      labelText: "Confirm Password",
                      border: const OutlineInputBorder(),
                      suffixIcon: IconButton(
                        onPressed: () {
                          if (_hiden) {
                            setState(() => _hiden = false);
                          } else {
                            setState(() => _hiden = true);
                          }
                        },
                        icon: Icon(
                          _hiden ? Icons.visibility_off : Icons.visibility,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 14.0),
                  SizedBox(
                    height: 50.0,
                    width: double.infinity,
                    child: CupertinoButton(
                      color: Colors.blue,
                      child: const Text('Sing Up'),
                      onPressed: () {
                            login();
                      },
                    ),
                  ),
                  //const SizedBox(height: 5.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text('Don\'t has an account?'),
                      TextButton(
                        onPressed: () {
                          // Navigator.pop(context);
                            Navigator.push(context,
                            MaterialPageRoute(builder: (context) =>
                            const LoginScreen(),),);
                        },
                        child: const Text('Sing In'),
                      )
                    ],
                  ),
                  Text("Result Register : "+userRegister)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

