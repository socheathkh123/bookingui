class Hotel {
  String imageUrl;
  String title;
  String description;
  int price;
  double rating;

  Hotel({required this.description, required this.imageUrl, required this.price, required this.rating, required this.title});
}

final List<Hotel> hotels = [
  Hotel(
    imageUrl: 'images/RoomType/hotel01.jpg',
    title: 'Single',
    description: 'Room N101',
    price: 180,
    rating: 4.5,
  ),
  Hotel(
    imageUrl: 'images/RoomType/hotel02.jpg',
    title: 'Double',
    description: 'Room N102',
    price: 180,
    rating: 4.5,
  ),
  Hotel(
    imageUrl: 'images/RoomType/hotel03.jpg',
    title: 'Single',
    description: 'Room N103',
    price: 180,
    rating: 4.5,
  ),
  Hotel(
    imageUrl: 'images/RoomType/hotel04.jpg',
    title: 'Double',
    description: 'Room N104',
    price: 180,
    rating: 4.5,
  )
];
