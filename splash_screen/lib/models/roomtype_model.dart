class RoomTypes {
  String imageUrl;
  String RoomType;

  RoomTypes({required this.imageUrl, required this.RoomType});
}

final List<RoomTypes> roomtypes = [
  RoomTypes(
    imageUrl: 'images/RoomType/Superior Double Room.jpg',
    RoomType: 'Superior Double Room',
  ),
  RoomTypes(
    imageUrl: 'images/RoomType/Deluxe Twin Room.jpg',
    RoomType: 'Deluxe Twin Room',
  ),
  RoomTypes(
    imageUrl: 'images/RoomType/Double Junior Suite.jpg',
    RoomType: 'Double Junior Suite',
  ),
  RoomTypes(
    imageUrl: 'images/RoomType/Twin Junior Suite.jpg',
    RoomType: 'Twin Junior Suite',
  ),
  RoomTypes(
    imageUrl: 'images/RoomType/Katari Suite.jpg',
    RoomType: 'Katari Suite',
  ),
  RoomTypes(
    imageUrl: 'images/RoomType/Deluxe Double Room.jpg',
    RoomType: 'Deluxe Double Room',
  )

];
