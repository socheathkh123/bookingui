import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:splash_screen/homepage.dart';
import 'package:splash_screen/register_screen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _hiden = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(padding: EdgeInsets.only(top: 50.0)),
                Container(
                  child: Image.asset("images/logo.png"),
                ),
                Padding(padding: EdgeInsets.only(top: 20.0)),
                Text(
                  "PHNOM PENH KATARI HOTEL",
                  style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                      color: Color(0xFFECB52E)
                  ),
                ),
                const SizedBox(height: 50.0),
                const TextField(
                  style: TextStyle(fontSize: 18.0),
                  decoration: InputDecoration(
                    suffixIcon: Icon(Icons.person),
                    hintText: 'Enter Your Email',
                    labelText: "Email",
                    border: OutlineInputBorder(),
                  ),
                ),
                const SizedBox(height: 16.0),
                TextField(
                  obscureText: _hiden,
                  style: const TextStyle(fontSize: 18.0),
                  decoration: InputDecoration(
                    hintText: 'Enter Your Password',
                    labelText: "Password",
                    border: const OutlineInputBorder(),
                    suffixIcon: IconButton(
                      onPressed: () {
                        if (_hiden) {
                          setState(() => _hiden = false);
                        } else {
                          setState(() => _hiden = true);
                        }
                      },
                      icon: Icon(
                        _hiden ? Icons.visibility_off : Icons.visibility,
                      ),
                    ),
                  ),
                ),
                //const SizedBox(height: 2.0),
                Container(
                  margin: const EdgeInsets.only(left: 180.0),
                  child: TextButton(
                    child: const Text('Forget Password ?'),
                    onPressed: () {},
                  ),
                  // const Text('forget password?'),
                ),
                const SizedBox(height: 6.0),
                SizedBox(
                  height: 50.0,
                  width: double.infinity,
                  child: CupertinoButton(
                    color: Colors.blue,
                    child: const Text('Sing In'),
                    onPressed: () {
                       //Navigator.pop(context);
                        Navigator.push(context,
                        MaterialPageRoute(builder: (context) =>
                        const HomePage(),),);
                    },
                  ),
                ),
                //const SizedBox(height: 16.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text('Don\'t has an account?'),
                    TextButton(
                      onPressed: () {
                        // Navigator.pop(context);
                        Navigator.push(context,
                         MaterialPageRoute(builder: (context) =>
                         const RegisterScreen(),),);
                      },
                      child: const Text('Sing Up'),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}