
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:splash_screen/login_screen.dart';


import 'favorites_page.dart';
import 'my_booking-page.dart';
import 'history_booking_page.dart';
import 'location_page.dart';
import 'about_page.dart';
import 'logout_page.dart';
import 'settings_page.dart';
import 'my_account_page.dart';
import 'dashboardpage.dart';
import 'navigation_drawer_screen.dart';


class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  var currentPage = DrawerSections.dashboard;

  int _currentIndex = 0;
  
  final tabs =[
    DashboardPage(),
    FavoritesPage(),
    MyBookingPage(),
    MyAccountPage(),
  ];

  @override
  Widget build(BuildContext context) {
    final container;
    if (currentPage == DrawerSections.dashboard) {
      container = DashboardPage();
    } else if (currentPage == DrawerSections.myaccount) {
      container = MyAccountPage();
    } else if (currentPage == DrawerSections.mybooking) {
      container = MyBookingPage();
    } else if (currentPage == DrawerSections.historybooking) {
      container = HistoryBookingPage();
    } else if (currentPage == DrawerSections.location) {
      container = LocationPage();
    } else if (currentPage == DrawerSections.settings) {
      container = SettingsPage();
    } else if (currentPage == DrawerSections.about) {
      container = AboutPage();
    } else if (currentPage == DrawerSections.logout) {
      container = LoginScreen();
    }

    return Scaffold(

       body:tabs[_currentIndex],

      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        type: BottomNavigationBarType.fixed,
        backgroundColor: Color(0xFF263171),
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('Home'),
              //color: Color(0xFFECB52E)
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.search),
              title: Text('Search'),
              //backgroundColor: Colors.blue
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.camera),
              title: Text('Camera'),
              //backgroundColor: Colors.blue
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.person),
              title: Text('Profile'),
              //backgroundColor: Colors.blue
          ),
        ],
        onTap: (index){
          setState(() {
            _currentIndex = index;
          });
        },
      ),


    );
  }

  Widget MyDrawerList() {
    return Container(
      padding: EdgeInsets.only(
        top: 15,
      ),
      child: Column(
        // shows the list of menu drawer
        children: [
          menuItem(1, "Dashboard", Icons.dashboard_outlined,
              currentPage == DrawerSections.dashboard ? true : false),
          menuItem(2, "My Account", Icons.people_alt_outlined,
              currentPage == DrawerSections.myaccount ? true : false),
          menuItem(3, "My Booking", Icons.event,
              currentPage == DrawerSections.mybooking ? true : false),
          menuItem(4, "History Booking", Icons.notes,
              currentPage == DrawerSections.historybooking ? true : false),
          menuItem(5, "Location", Icons.notifications_outlined,
              currentPage == DrawerSections.location ? true : false),
          menuItem(6, "Settings", Icons.settings_outlined,
              currentPage == DrawerSections.settings ? true : false),
          menuItem(7, "About", Icons.privacy_tip_outlined,
              currentPage == DrawerSections.about ? true : false),
          menuItem(8, "LogOut", Icons.feedback_outlined,
              currentPage == DrawerSections.logout ? true : false),
        ],
      ),
    );
  }

  Widget menuItem(int id, String title, IconData icon, bool selected) {
    return Material(
      color: selected ? Colors.grey[300] : Colors.transparent,
      child: InkWell(
        onTap: () {
          Navigator.pop(context);
          setState(() {
            if (id == 1) {
              currentPage = DrawerSections.dashboard;
            } else if (id == 2) {
              currentPage = DrawerSections.myaccount;
            } else if (id == 3) {
              currentPage = DrawerSections.mybooking;
            } else if (id == 4) {
              currentPage = DrawerSections.historybooking;
            } else if (id == 5) {
              currentPage = DrawerSections.location;
            } else if (id == 6) {
              currentPage = DrawerSections.settings;
            } else if (id == 7) {
              currentPage = DrawerSections.about;
            } else if (id == 8) {
              currentPage = DrawerSections.logout;
            }
          });
        },
        child: Padding(
          padding: EdgeInsets.all(15.0),
          child: Row(
            children: [
              Expanded(
                child: Icon(
                  icon,
                  size: 20,
                  color: Colors.black,
                ),
              ),
              Expanded(
                flex: 3,
                child: Text(
                  title,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

enum DrawerSections {
  dashboard,
  myaccount,
  mybooking,
  historybooking,
  location,
  settings,
  about,
  logout,
}


